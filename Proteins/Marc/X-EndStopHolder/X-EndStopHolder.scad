// Marc BERLIOUX 2014
// license : "Make it Easy"

totalHeight=30;

difference(){

translate([0,0,totalHeight/2])
linear_extrude(height=totalHeight, center = true, convexity=5) import("X-EndStopHolder.dxf", layer="dessus", $fn=64);

translate([0,0,40])
rotate([90,180,180])
linear_extrude(height=totalHeight+10, center = true, convexity=5) import("X-EndStopHolder.dxf", layer="face", $fn=64);

translate([12.5,-6.5,18])
cube([20,20,30],center=true);

translate([0,-13,23])
cube([20,20,30],center=true);

translate([0,-7.5,18])
cube([20,9,30],center=true);

translate([6.5,-11.5,21])
rotate([0,45,0])
cube([20,9,30],center=true);

}

