// Marc BERLIOUX 2015
// license : "Make it Easy"

stencilHeight=0.6;

translate([0,0,stencilHeight/2])
difference(){
linear_extrude(height=stencilHeight, center = true, convexity=5) import("LogoLOG.dxf",layer="stencil", $fn=64);

linear_extrude(height=stencilHeight, center = true, convexity=5) import("LogoLOG.dxf",layer="PRINCIPAL", $fn=64);
}