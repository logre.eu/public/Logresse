// Marc BERLIOUX 2014
// license : "Make it Easy"

plateHeight=1.5;
edgeHeight=5;

translate([0,0,plateHeight/2])
linear_extrude(height=plateHeight, center = true, convexity=5) import("TopCapPlain.dxf",layer="Exterieur", $fn=64);

translate([0,0,edgeHeight/2+plateHeight])
linear_extrude(height=edgeHeight, center = true, convexity=5) import("TopCapPlain.dxf",layer="PRINCIPAL", $fn=64);
